import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './components/routes/inicio/inicio.component';
import { PeliculasComponent } from './components/routes/peliculas/peliculas.component';
import { SeriesComponent } from './components/routes/series/series.component';
import { IngresarComponent } from './components/routes/ingresar/ingresar.component';

const routes: Routes = [
  {
    path: 'inicio', component: InicioComponent
  },
  {
    path: 'peliculas', component: PeliculasComponent
  },
  {
    path: 'series', component: SeriesComponent
  },
  {
    path: 'ingresar', component: IngresarComponent
  },
  {
    path: '**',//Este es cuando me dé error, o la página sea diferente a las referenciadas
    redirectTo: 'inicio'
  },
  {
    path: ' ',//Este es cuando esté vacío
    redirectTo: 'inicio'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
